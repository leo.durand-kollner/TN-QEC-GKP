using ITensors

function createMPS(N::Integer)
    """
    Create MPS for the [N]-repetition code ([N] must be odd)
    """
    if (N%2 == 0)
        throw(DomainError(N, "argument must be odd"))
    end

    sites = siteinds("Qubit", 2*N-1) # How many qubit we have (size 2 indexes)
    qubits = sites[1:N]
    stabilizers = sites[N+1:2*N-1]
    states = ["0" for i in 1:length(sites)] # State of each index (we begin in |0⟩)
    ψ = MPS(sites, states)

    ψ, qubits, stabilizers
end

function encode(ψ, qubits)
    """
    Prepare logical state |0⟩
    Assuming no errors during this process
    """
    for q in qubits[2:end]
        CX = op("CNOT", qubits[1], q)
        ψ = apply(CX, ψ)
    end
    
    ψ
end

function bit_flip_channel(ψ, qubits, p_err)
    """
    Noisy bit flip channel with probability [p_err] to flip each qubit
    """
    for q in qubits
        if rand() < p_err
            ψ = apply(op("X", q), ψ)
        end
    end
    ψ
end

function measure_stabilizers(ψ, qubits, stabilizers)
    N = length(qubits)

    for i in 1:length(stabilizers)
        CX1 = op("CNOT", qubits[i], stabilizers[i])
        CX2 = op("CNOT", qubits[i+1], stabilizers[i])
        ψ = apply(CX1, ψ)
        ψ = apply(CX2, ψ)
    end

    ψ
end

function bitflip_3repetition(N, p_err)
    errors = 0
    for i in 1:N
        ψ, qubits, stabilizers = createMPS(3)
        ψ = encode(ψ, qubits)
        ψ = bit_flip_channel(ψ, qubits, p_err)
        ψ = measure_stabilizers(ψ, qubits, stabilizers)
        orthogonalize!(ψ,1)

        s1, s2 = expect(ψ, "Z", sites=4:5)

        if s1 == -1.0 && s2 == 1.0
            ψ = apply(op("X", qubits[1]), ψ)
        elseif s1 == 1.0 && s2 == -1.0
            ψ = apply(op("X", qubits[3]), ψ)
        elseif s1 == -1.0 && s2 == -1.0
            ψ = apply(op("X", qubits[2]), ψ)
        end

        errors += (reduce(*, expect(ψ, "Z", sites=1:3)) == -1)
    end
    errors
end